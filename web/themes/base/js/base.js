/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.exampleBehavior = {
    attach: function (context, settings) {
      //alert("I'm alive!");
    }
  };

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.cookies = {
        attach: function (context, settings) {
            $(document).ready(function() {
                $('.decline-button').on('click', function(){
                    $('#sliding-popup').remove();
                });
            });
        }
    };

    /**
   * Use this behavior as a template for custom Javascript.
   */
         Drupal.behaviors.scrollTo = {
          attach: function (context, settings) {
              $('.scroll-wrapper .scroll-down').on('click', function(){
                  $('html,body').animate({

                    scrollTop: $(".view-mode-full").offset().top }, {duration: 500 } );
              });
          }
      };

  /**

     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.popUp = {
        attach: function (context, settings) {
            $(context).find('#block-popup').once('ifPopUp').each(function () {
                var cookiePopUpCheck = $.cookie("popUp-check");

                function checkCookie() {
                    cookiePopUpCheck = $.cookie("popUp-check", 1, {expires: 2});
                }
                if (!cookiePopUpCheck) {
                    $('#block-popup').foundation('open');
                }

                $('#close-pop-up').on('click', function () {
                    $("#block-popup").on("closed.zf.reveal", function (e) {
                        checkCookie();
                    });
                    $('#block-popup').foundation('close');
                });
            });
        }
    }

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.slides = {
        attach: function (context, settings) {
            $(context).find('.field-name-field-slides.swiper-container').once('ifLatestSlider').each(function (){
                var slides = new Swiper (this, {
                    autoplay: true,
                    loop: true,
                    effect: 'fade',
                    pagination: {
                        el: '.swiper-pagination1',
                        type: 'bullets',
                        clickable: true,
                        renderBullet: function (index, className) {
                          return '<span class="' + className + '">0' + (index + 1) + '<span class="circle left"><span></span></span><span class="circle right"><span></span></span></span>';
                        },
                    },
                });
            });

            $(context).find('.field-name-field-images.swiper-container').once('ifLatestSlider').each(function () {
              var slides = new Swiper(this, {
                autoplay: true,
                slidesPerView: 'auto',
                loop: true,
                effect: 'slide',
                spaceBetween: 19,
                pagination: {
                    el: '.swiper-pagination2',
                    type: 'bullets',
                    clickable: true,
                    renderBullet: function (index, className) {
                      return '<span class="' + className + '">0' + (index + 1) + '<span class="circle left"><span></span></span><span class="circle right"><span></span></span></span>';
                    },
                },
              });
            });
        }
    };

    /**
     * offCanvas behaviors
     */
    Drupal.behaviors.offCanvas = {
        attach: function (context, settings) {
            $(context).find('#offCanvasRightOverlap').once('ifRightCanvas').each(function () {
                $menuButton = $('#menuButton');
                $(this).on("opened.zf.offcanvas", function(e){
                    $menuButton.addClass('is-active');
                    $('body, html').addClass('menu-show');
                });
                $(this).on("closed.zf.offcanvas", function(e){
                    $menuButton.removeClass('is-active');
                    $('body, html').removeClass('menu-show');
                });
            });
        }
    };

    /**
     * Product effect behaviors
     */
    Drupal.behaviors.productEffect = {
      attach: function (context, settings) {
        $(document).ready(function () {
          function storeTabs($tabs, $destination) {
            // measure width
            $tabs.each(function () {
              var width = $(this).outerWidth(true);
              $(this).data('width', width);
            });
            $tabs.prependTo($destination);
          }

          function makeTabsResponsive($element) {

            var $tabs = $element.find('li');
            var $firstTab = $tabs.first();

            var individualTabHeight = $firstTab.outerHeight();
            var tabsHeight = $element.outerHeight();

            if (tabsHeight > individualTabHeight) {

              // get y pos of first tab
              var firstTabPos = $firstTab.offset();

              var thisTabPos;
              $tabs.each(function () {

                var $thisTab = $(this);

                thisTabPos = $thisTab.offset();

                if (thisTabPos.top > firstTabPos.top) {

                  var $dropdown = $element.find('.responsivetabs-more');

                  if (!$dropdown.length) {
                    var dropdownMarkup = '<li class="dropdown responsivetabs-more">' +
                      '<a href="#" class="dropdown-toggle" data-toggle="dropdown">...</a>' +
                      '<ul class="dropdown-menu dropdown-menu-right">' +
                      '</ul></li>';
                    $dropdown = $(dropdownMarkup);
                    $element.append($dropdown);

                  }

                  var $previousTab = $thisTab.prev();
                  var $followingTabs = $thisTab.nextAll().not('.dropdown');

                  var $destination = $('.dropdown-menu', $dropdown);

                  if (!$thisTab.hasClass('dropdown')) {
                    storeTabs($followingTabs, $destination);
                    storeTabs($thisTab, $destination);
                  }
                  storeTabs($previousTab, $destination);

                  return;

                }

              });

            } else {



              // check if enough space to move a menu item back out of "..."


              // get parent width
              var parentWidth = $element.parent().width();
              var tabSetWidth = 0;
              var xPxAvailable;

              // calculate total width of tab set (can't just use width of ul because it is 100% by default)
              $element.children('li').each(function () {
                tabSetWidth += $(this).outerWidth(true);
              });

              // calculate available horizontal space
              xPxAvailable = parentWidth - tabSetWidth;



              $element.find('.dropdown-menu li').each(function () {
                if ($(this).data('width') <= xPxAvailable) {
                  $(this).insertBefore($element.find('.responsivetabs-more'));
                  xPxAvailable -= $(this).data('width');
                } else {
                  return false;
                }
              });

              // if no menu items left, remove "..."
              if (!$element.find('.dropdown-menu li').length) {
                $element.find('.responsivetabs-more').remove();
              }
            }


          }


          $.fn.responsiveTabs = function () {

            this.each(function () {
              var tabs = $(this);
              makeTabsResponsive(tabs);
              $(window).resize(function () {
                makeTabsResponsive(tabs);
              });
            });

            return this;

          };

          $('#example-tabs').responsiveTabs();
        });
      }
    };

    /**
     * offCanvas behaviors
     */
    Drupal.behaviors.clickdrop = {
      attach: function (context, settings) {
        $(document).ready(function () {
          $('.dropdown-toggle').on('click', function (e) {
            e.preventDefault();
            $(this).parent('li').toggleClass('open');
          });
          $('.dropdown-menu .tabs-title').on('click', function (e) {
            e.preventDefault();
            $('.responsivetabs-more').removeClass('open');
          });
        });
      }
    };

    Drupal.behaviors.scrolllist = {
        attach: function (context, settings) {
        $('.field-name-field-method .field-item:first-child').addClass('active');
        $(window).on( 'scroll', function(){
          $('.field-name-field-method .field-item').each(function(index){
              var $active_count = $('.active').length;
              var $line_elem = $('.content-viewport').offset().top;
              var $target_elem = $(this).find('span.oval').offset().top;
             if($line_elem > $target_elem){
                $('.field-name-field-method .field-item:first-child').removeClass('active');
                $(this).addClass('active');
                $(this).siblings().removeClass('active');
             }else{
               $(this).removeClass('active');  
             }
             if($active_count < 2  ){
              $('.field-name-field-method .field-item:first-child').addClass('active');
             }else{
              $('.field-name-field-method .field-item:first-child').removeClass('active');
             }
           });
        });
      }
    }


})(jQuery, Drupal);

